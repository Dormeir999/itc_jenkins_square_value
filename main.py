"""
gets a number as first command line
argument and prints a sequence from 0 to that number-1 and the square value of it
Author: Dor Meir
"""
import sys
import urllib.request
import os

def get_html(list_of_urls):
    for url in list_of_urls:
        print('-------------------------------- in get_html')
        print(type(url))
        html = urllib.request.urlopen(str(url))
        mybytes = html.read()
        #mystr = mybytes.decode("utf8")
        html.close()
        print("The url is:")
        print(url)
        print(mybytes)


def main():
    print('---------------------------- in main')
    print(os.environ['p'])
    list_of_urls = os.environ['p'].split(",")
    print(list_of_urls)
    #list_of_urls = "https://www.google.com/,https://www.ynet.co.il/,https://www.walla.co.il/".split(",")
    get_html(list_of_urls)


if __name__ == '__main__':
    main()
